// @license magnet:?xt=urn:btih:87f119ba0b429ba17a44b4bffcab33165ebdacc0&dn=freebsd.txt FreeBSD
function reset(){
	var x = 0;
	var y = 0;
	var xy = null;

	for(y = 0; y < 15; y++){
		for(x = 0; x < 80; x++){
			xy = "" + x + "-" + y;
			el = document.getElementById(xy);
			if(y <= 2){
				el.innerHTML = "&nbsp;";
			}
			el.setAttribute("style", "background-color: #F5E8B5; color: #BD492F;");

		}
	}

}
function render(){
	reset();

	var failsafe1 = "SEGMENTATAION FAULT (CORE DUMPED)";
	var failsafe2 = "TEXT CAN ONLY BE EQUAL TO OR LESS THAN 80 CHARS";
	var failsafe3 = "UNICODE DETECTED; PUNCHCARD REJECTED.";
	var using = null;
	var set = 0;

	// various iterators
	var subarlen = 0;
	var i = 0;
	var j = 0;
	var k = 0;

	// coordinates
	var x = 0; 
	var y = 0;
	var xy = null;

	var input = document.getElementById('textbox').value;

	/* INPUT VALIDATION
	 * if the string is too long or too short, use a default input
	 * if unicode is detected, use a failsafe
	 * if the above are untrue, use the input string
	 * failsafe1 is unused
	 */

	if(input.length <= 80 && input.length > 0){	
		using = input;
		set = 1;
	} else if (set != 1) {
		using = failsafe2;
		set = 1;
	}

	for(i = 0; i < input.length; i++){
		if(input.charCodeAt(i) > 255 && SET != 1){
			using = failsafe3;
			set = 1;
		}
	}

	// capitalize string append 80-strlen spaces
	using = using.padEnd(80, " ");
	using = using.toUpperCase();

	/* CARD KEY 
      ________________________________________________________________
0    /&-0123456789ABCDEFGHIJKLMNOPQR/STUVWXYZ:#@'="[.<(+|]$*);^\,%_>?
1   / O           OOOOOOOOO                        OOOOOO
2  |   O                   OOOOOOOOO                     OOOOOO
3  |    O                           OOOOOOOOO                  OOOOOO
4  |                                                                 
5  |     O        O        O        O
6  |      O        O        O        O       O     O     O     O
7  |       O        O        O        O       O     O     O     O
8  |        O        O        O        O       O     O     O     O
9  |         O        O        O        O       O     O     O     O
10 |          O        O        O        O       O     O     O     O
11 |           O        O        O        O       O     O     O     O
12 |            O        O        O        O OOOOOOOOOOOOOOOOOOOOOOOO
13 |             O        O        O        O
14 |__________________________________________________________________

*/

	/* ENCODING DEFINITIONS
	 * y is 0-14. Rows 4 and 14 are unused. 
	 * x is 0-79. All can be used. 
	 * x position comes from the position of the char in the input string
	 * y positions come from the array
	 * if multiple y-s are defined, multiple punches occur
	 */


	var encodings = [
		["&", 1 ],
		["-", 2 ],
		["0", 3 ],
		["1", 5 ],
		["2", 6 ],
		["3", 7 ],
		["4", 8 ],
		["5", 9 ],
		["6", 10 ],
		["7", 11 ],
		["8", 12 ],
		["9", 13 ],
		["A", 1, 5],
		["B", 1, 6],
		["C", 1, 7],
		["D", 1, 8],
		["E", 1, 9],
		["F", 1, 10],
		["G", 1, 11],
		["H", 1, 12],
		["I", 1, 13],
		["J", 2, 5],
		["K", 2, 6],
		["L", 2, 7],
		["M", 2, 8],
		["N", 2, 9],
		["O", 2, 10],
		["P", 2, 11],
		["Q", 2, 12],
		["R", 2, 13],
		["/", 3, 5], 
		["S", 3, 6], 
		["T", 3, 7], 
		["U", 3, 8], 
		["V", 3, 9], 
		["W", 3, 10], 
		["X", 3, 11], 
		["Y", 3, 12], 
		["Z", 3, 13], 
		[":", 12, 6],
		["#", 12, 7],
		["@", 12, 8],
		["'", 12, 9],
		["=", 12, 10],
		['"', 12, 11],
		["[", 1, 12, 6], 
		[".", 1, 12, 7], 
		["<", 1, 12, 8], 
		["(", 1, 12, 9], 
		["+", 1, 12, 10], 
		["|", 1, 12, 11], 
		["]", 2, 12, 6], 
		["$", 2, 12, 7], 
		["*", 2, 12, 8], 
		[")", 2, 12, 9], 
		[";", 2, 12, 10], 
		["^", 2, 12, 11], 
		["\\", 3, 12, 6], 
		[",", 3, 12, 7], 
		["%", 3, 12, 8], 
		["_", 3, 12, 9], 
		[">", 3, 12, 10], 
		["?", 3, 12, 11], 
	];

	/* ITERATE OVER STRING AND MODIFY HTML */
	var chararr = using.split('');

	for(i = 0; i < chararr.length; i++){
		for(j = 0; j < encodings.length; j++){
			if(chararr[i] === encodings[j][0]){
				for(k = 1; k < encodings[j].length; k++){
					xy = "" + i + "-" + encodings[j][k];
					document.getElementById(xy).setAttribute("style", "background-color: #000000; color: #000000;");
					xy = "" + i + "-" + 0;
					document.getElementById(xy).innerText = encodings[j][0] + "";

				}
			}
		}
	}

}

// reset on page load
document.addEventListener('DOMContentLoaded', function(){
	var card = document.getElementById("card")
	var wpx = window.getComputedStyle(card, null)["width"];
	wpx = wpx.substring(0, wpx.length -2);
	var hpx = Math.trunc((wpx*(83/187)));
	document.getElementById("card").setAttribute("style", "height: " + hpx + "px" + ";");
	console.log("reset");
	reset();
}, false);
// @license-end
