This is a somewhat broken web app that can be used to (slowly) generate an 80 column, 64 character punch card. This is not a real punch card generator, it's simply yet another webtoy.

Here is another [punchedcard generator that can generate a .png](http://www.kloth.net/services/cardpunch.php).

# FORMAT

I chose the `EBCD` character set because this set contains the most typable characters while having the least number of "untypable and therefore unprintable characters". The 64 characters in the `EBCD` set contain:

```
&-0123456789ABCDEFGHIJKLMNOPQR/STUVWXYZ:#@'="[.<(+|]$*);^\,%_>? 
```

My card "row" format is slightly different from the standard but the cards are still compliant. 

The way a punch card works is by punching holes at a series of coordinates. The text written at line 0 is to make it human readable. Lines 4 and 14 only exist to indicate column numbers. This means that lines 0, 4, and 14 are not punched. 

```
      ________________________________________________________________
0    /&-0123456789ABCDEFGHIJKLMNOPQR/STUVWXYZ:#@'="[.<(+|]$*);^\,%_>?
1   / O           OOOOOOOOO                        OOOOOO
2  |   O                   OOOOOOOOO                     OOOOOO
3  |    O                           OOOOOOOOO                  OOOOOO
4  |                                                                 
5  |     O        O        O        O
6  |      O        O        O        O       O     O     O     O
7  |       O        O        O        O       O     O     O     O
8  |        O        O        O        O       O     O     O     O
9  |         O        O        O        O       O     O     O     O
10 |          O        O        O        O       O     O     O     O
11 |           O        O        O        O       O     O     O     O
12 |            O        O        O        O OOOOOOOOOOOOOOOOOOOOOOOO
13 |             O        O        O        O
14 |__________________________________________________________________
```

For example: if you wanted to write "HELLO WORLD", you would send a series of holes to punch and a character to display. We can translate this string into a series of coordinates with a character attached. Some characters require only 1 punch, some require 2, and some require 3. 

```
// char, coordinates
H       (0,1)           (0,12)
E       (1,1)           (1,9)
L       (2,2)           (2,7)
L       (3,2)           (3,7)
O       (4,2)           (4,10)
[space] (null, null)
W       (6,3)           (6,10)
O       (7,2)           (7,10)
R       (8,2)           (8,13)
L       (9,2)           (9,7)
D       (10,1)          (10,8)
```

Simple math shows us how many possible characters we could have with 12 possible places to insert a punch: 

```
P(n) = n!
P(12) = 12! = 12*11*10*9*8*7*6*5*4*3*2*1 = 479001600 
```

But this does not include *a row without punches*, so the total number is 479,001,600+1. This is very large character set that is capable of representing all of the [currently implemented unicode 15.0.0 characters ](https://www.unicode.org/versions/Unicode15.0.0/) (149,186 characters)

My math might actually be wrong here. Submit a patch to correct me. 


# ISSUES 
non-issues include the following:

- Unicode is obviously not
supported.
- ASCII characters (there are 128 of them) are not supported.  Only the 64 characters above are supported. Any unsupported ASCII-128 characters (such as \~, !, {, }) will fail elegantly to whitespace.

actual issues include the following:

- I have not tested with non-US keyboards to determine if non-English systems "upgrade" standard ASCII characters to unicode or if they leave them alone.
- you need a big screen. In order to get the dimensions of the punchcard correct while still scaling to the client window I had to break the "mobile first, mobile only, mobile forever" devs. The interim solution is to stop being a digital serf and to buy a real computer.
- Chrome does not respect the spacing between the columns on normal sized screens. The interim solution for this is to use a web browser based on a standards respecting web engine or zoom way out.
- reload the page on window resizes or things break

# LINKS

[Lots of help from the Punched Card Collection](https://homepage.divms.uiowa.edu/~jones/cards/codes.html)

Fonts used: [IBM Plex](https://github.com/IBM/plex) and [Keypunch](https://fontlibrary.org/en/font/keypunch029), both of which are licensed under the OFL.
